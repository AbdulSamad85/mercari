package com.echo.mercari.activity;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.crashlytics.android.Crashlytics;
import com.echo.mercari.R;
import com.echo.mercari.adapter.SimpleFragmentPagerAdapter;
import com.echo.mercari.databinding.ActivityDrawerBinding;
import com.echo.mercari.fragment.base.GridFragment;
import com.echo.mercari.loader.JsonAsyncTaskLoader;
import com.echo.mercari.models.Item;
import com.echo.mercari.models.ItemResponseModel;
import com.echo.mercari.utility.DialogUtils;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DrawerActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, GridFragment.ItemFragmentCallbacks, LoaderManager.LoaderCallbacks<HashMap<String, ItemResponseModel>> {

    private List<String> ItemFileNamesList;
    private ActivityDrawerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_drawer);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initFab();
        initDrawer(toolbar);
        initializeResources();
    }

    private void initFab(){
        FloatingActionLayout fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


    private void initDrawer(Toolbar toolbar){
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.mipmap.icon_launcher, getTheme());
//        toggle.setDrawerIndicatorEnabled(false);
//        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void initializeResources() {
        try {
            String[] stringArray = getResources().getStringArray(R.array.item_file_names);
            if (stringArray.length > 0) {
                ItemFileNamesList = Arrays.asList(stringArray);
                getSupportLoaderManager().initLoader(0, null, this);
            }
        } catch (Resources.NotFoundException ex) {
            Crashlytics.logException(ex);
            ex.printStackTrace();
            DialogUtils.showToast(DrawerActivity.this, getString(R.string.no_items_available));
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
//            // app has just one screen, no need to do anything
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } else if (id == R.id.nav_quite) {
            finish();
            return false;
        }
        return true;
    }

    @Override
    public void onItemClicked(Item clickedItem) {
        DialogUtils.showToast(DrawerActivity.this, clickedItem.getName() + " "+getString(R.string.clicked));
    }


    @NonNull
    @Override
    public Loader<HashMap<String, ItemResponseModel>> onCreateLoader(int id, @Nullable Bundle args) {
        showProgressDialog();
        return new JsonAsyncTaskLoader(DrawerActivity.this, ItemFileNamesList);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<HashMap<String, ItemResponseModel>> loader, final HashMap<String, ItemResponseModel> data) {
        // adding a short delay for smoothness
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initAdapter(data);
            }
        },200);

        hideProgressDialog();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<HashMap<String, ItemResponseModel>> loader) {

    }

    private void initAdapter(HashMap<String,ItemResponseModel> map){
        if(map != null && map.keySet().size() >0) {

            List<String> titlesList = new ArrayList<>(map.keySet().size());
            List<List<Item>> dataSource = new ArrayList<>();

            for (String title : map.keySet()) {
                List<Item> itemsList = map.get(title).getData();
                dataSource.add(itemsList);

                title = title.split("/")[1];
                title = title.replaceAll(".json","");
                title = title.substring(0, 1).toUpperCase() + title.substring(1).toLowerCase();
                titlesList.add(title);
            }

            ViewPager viewPager = findViewById(R.id.viewpager);
            viewPager.setAdapter(new SimpleFragmentPagerAdapter(getSupportFragmentManager(), titlesList, dataSource));
            viewPager.setOffscreenPageLimit(map.keySet().size());

            SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
            viewPagerTab.setViewPager(viewPager);
        } else {
            DialogUtils.showToast(DrawerActivity.this, getString(R.string.no_items_available));
        }
    }
}
