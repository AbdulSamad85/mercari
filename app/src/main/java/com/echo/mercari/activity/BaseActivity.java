package com.echo.mercari.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.echo.mercari.dialog.CustomProgressDialog;


/**
 * Created by abdulsamad on 04/10/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected CustomProgressDialog mCustomProgressDialog;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgressDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing()) {
            hideProgressDialog();
        }
        mCustomProgressDialog = CustomProgressDialog.show(this);
    }

    public void hideProgressDialog() {
        if (mCustomProgressDialog != null && mCustomProgressDialog.isShowing())
            mCustomProgressDialog.dismiss();
    }
}
