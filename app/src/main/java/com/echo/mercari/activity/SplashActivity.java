package com.echo.mercari.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;

import com.echo.mercari.R;
import com.echo.mercari.databinding.SplashActivityLayoutBinding;

/**
 * Created by Tayyab on 1/15/2018.
 */

public class SplashActivity extends BaseActivity {

    private static final int ANIMATION_DURATION = 1500;
    private  SplashActivityLayoutBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splash_activity_layout);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //to remove the action bar (title bar)
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {

             navigateToDrawerActivity();
            }
        }, ANIMATION_DURATION);
        animateLogo();
    }

    private void animateLogo(){
        binding.logo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.logo_animation));
    }
    private void navigateToDrawerActivity(){

        Intent intent = new Intent(SplashActivity.this, DrawerActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.splash_fade_in, R.anim.splash_fade_out);
        finish();
    }
}
