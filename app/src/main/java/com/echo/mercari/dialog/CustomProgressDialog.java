package com.echo.mercari.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.echo.mercari.R;


/**
 * Created by zaid on 29/05/2015.
 */
public class CustomProgressDialog extends Dialog {

//    public static CustomProgressDialog show(final Context context) {
//        return show(context, title, message, false);
//    }
//
//    public static CustomProgressDialog show(final Context context,
//                                            final CharSequence title, final CharSequence message,
//                                            final boolean indeterminate) {
//        return show(context, title, message, indeterminate, false, null);
//    }
//
//    public static CustomProgressDialog show(final Context context,
//                                            final CharSequence title, final CharSequence message,
//                                            final boolean indeterminate, final boolean cancelable) {
//        return show(context, title, message, indeterminate, cancelable, null);
//    }
//
//    public static CustomProgressDialog show(final Context context,
//                                            final CharSequence title, final CharSequence message,
//                                            final boolean indeterminate, final boolean cancelable,
//                                            final OnCancelListener cancelListener) {
//        CustomProgressDialog dialog = new CustomProgressDialog(context);
//        dialog.setTitle(title);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.setOnCancelListener(cancelListener);
//        LayoutInflater inflator =  LayoutInflater.from(context);
//        View v = inflator.inflate(R.layout.my_progress_bar, null);
//        dialog.setContentView(v);
//        dialog.show();
//
//        return dialog;
//    }

    public static CustomProgressDialog show(final Context context) {
        CustomProgressDialog dialog = new CustomProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        LayoutInflater inflator =  LayoutInflater.from(context);
        View v = inflator.inflate(R.layout.my_progress_bar, null);
        dialog.setContentView(v);
        dialog.show();
        return dialog;
    }

    private CustomProgressDialog(final Context context) {
        super(context, R.style.ProgressBarTheme);
    }
}
