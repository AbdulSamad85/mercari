package com.echo.mercari.listener;

/**
 * Created by zaid on 19/05/2015.
 */
public interface DialogListener {

    void onPositiveButtonClicked(String input);

    void onNegativeButtonClicked();
}
