package com.echo.mercari.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.crashlytics.android.Crashlytics;
import com.echo.mercari.R;
import com.echo.mercari.enums.User;
import com.echo.mercari.models.Item;
import com.echo.mercari.models.ItemResponseModel;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by abdulsamad on 14/03/2018.
 */

public class JsonAsyncTaskLoader extends AsyncTaskLoader<HashMap<String, ItemResponseModel>> {

    private HashMap<String, ItemResponseModel> loadedJsonMap;
    private List<String> fileNames;

    public JsonAsyncTaskLoader(Context context, List<String> fileNames) {
        super(context);
        if (fileNames == null || fileNames.size() == 0) {
            throw new RuntimeException("Please pass valid file names list");
        } else {
            this.fileNames = fileNames;
        }
    }

    @Override
    protected void onStartLoading() {
        if (loadedJsonMap != null && loadedJsonMap.keySet().size() == fileNames.size()) {
            // Use cached data
            deliverResult(loadedJsonMap);
        } else {
            // We have no data, so kick off loading it
            forceLoad();
        }
    }

    @Override
    public HashMap<String, ItemResponseModel> loadInBackground() {
        // This is on a background thread
        HashMap<String, ItemResponseModel> map = new LinkedHashMap<>();
        try {
            for (String jsonFileName : fileNames) {
                String json = loadJSONFromAssets(jsonFileName);
                ItemResponseModel model = new Gson().fromJson(json, ItemResponseModel.class);
                User user = getUserForFile(jsonFileName);
                for(Item item: model.getData()){
                    item.setPrimaryUser(user);
                }
                map.put(jsonFileName, model);
            }

        } catch (Exception ex) {
            Crashlytics.logException(ex);
//            // return null if some exception occurs
            return null;
        }
//         Check isLoadInBackgroundCanceled() to cancel out early
        return map;
    }

    @Override
    public void deliverResult(HashMap<String, ItemResponseModel> map) {
        // We’ll save the data for later retrieval
        loadedJsonMap = map;
        // We can do any pre-processing we want here
        // Just remember this is on the UI thread so nothing lengthy!
        super.deliverResult(loadedJsonMap);
    }


    private User getUserForFile(String fileName){
        User user;
        if(fileName.equalsIgnoreCase(getContext().getString(R.string.men_file))){
            user = User.MEN;
        } else if(fileName.equalsIgnoreCase(getContext().getString(R.string.women_file))){
            user = User.WOMEN;
        } else {
            user = User.ALL;
        }
        return user;
    }
    public String loadJSONFromAssets(String fileName) {
        String json = null;
        try {
            InputStream is = getContext().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            Crashlytics.logException(ex);
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}