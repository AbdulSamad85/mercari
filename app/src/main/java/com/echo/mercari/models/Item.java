package com.echo.mercari.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.echo.mercari.enums.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abdulsamad on 11/03/2018.
 */

public  class Item implements Parcelable {

    protected String id;
    protected String name;
    protected String status; // "on_sale / sold_out"
    @SerializedName("num_likes")
    @Expose
    protected long numLikes;
    @SerializedName("num_comments")
    @Expose
    protected long numComments;
    protected double price;
    protected String photo;
    protected User primaryUser;



    public Item(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getNumLikes() {
        return numLikes;
    }

    public void setNumLikes(long numLikes) {
        this.numLikes = numLikes;
    }

    public long getNumComments() {
        return numComments;
    }

    public void setNumComments(long numComments) {
        this.numComments = numComments;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public User getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(User primaryUser) {
        this.primaryUser = primaryUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.status);
        dest.writeLong(this.numLikes);
        dest.writeLong(this.numComments);
        dest.writeDouble(this.price);
        dest.writeString(this.photo);
        dest.writeInt(this.primaryUser == null ? -1 : this.primaryUser.ordinal());
    }

    protected Item(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.status = in.readString();
        this.numLikes = in.readLong();
        this.numComments = in.readLong();
        this.price = in.readDouble();
        this.photo = in.readString();
        int tmpPrimaryUser = in.readInt();
        this.primaryUser = tmpPrimaryUser == -1 ? null : User.values()[tmpPrimaryUser];
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}
