package com.echo.mercari.models;

import java.util.List;

/**
 * Created by abdulsamad on 14/03/2018.
 */

public class ItemResponseModel {
    private String result;
    private List<Item> data = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }
}
