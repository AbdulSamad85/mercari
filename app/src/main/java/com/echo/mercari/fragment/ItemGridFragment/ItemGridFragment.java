package com.echo.mercari.fragment.ItemGridFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.echo.mercari.R;
import com.echo.mercari.adapter.ItemGridAdapter;
import com.echo.mercari.databinding.FragmentGridBinding;
import com.echo.mercari.fragment.base.GridFragment;
import com.echo.mercari.models.Item;
import com.echo.mercari.utility.DialogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulsamad on 11/03/2018.
 */

public class ItemGridFragment extends GridFragment {
    protected static final String KEY_ITEMS =  "KEY_ITEMS";
    protected static final String TAG =  ItemGridFragment.class.getName();
    private ItemGridAdapter adapter;
    private List<Item> itemsList;

    FragmentGridBinding binding;
    public static ItemGridFragment newInstance(List<Item> items) {
        ArrayList<Item> arrayList = new ArrayList<>(items.size());
        arrayList.addAll(items);
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_ITEMS, arrayList);
        ItemGridFragment fragment = new ItemGridFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (binding == null) {
            binding = FragmentGridBinding.inflate(inflater, container, false);
            handleBundle();
        }
        return binding.getRoot();
    }

    private void handleBundle() {
        if(getArguments() != null && getArguments().containsKey(KEY_ITEMS)){
            itemsList = getArguments().getParcelableArrayList(KEY_ITEMS);
            if(itemsList != null && itemsList.size() >0){
                initGrid(itemsList);
            } else {
                DialogUtils.showToast(getActivity(), getString(R.string.no_items_available));
            }
        } else {
            Log.e(TAG,"pass the items list");
        }
    }

    public void initGrid(final List <Item> data) {
        adapter = new ItemGridAdapter(getActivity(), data);
        binding.gridView.setAdapter(adapter);
        binding.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener != null){
                    listener.onItemClicked(itemsList.get(position));
                }
            }
        });
    }
}
