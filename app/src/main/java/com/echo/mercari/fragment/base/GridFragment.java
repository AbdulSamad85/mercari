package com.echo.mercari.fragment.base;

import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.echo.mercari.models.Item;


/**
 * Created by applepc on 09/09/2017.
 */

public abstract class GridFragment extends BaseFragment {
    public ItemFragmentCallbacks listener;

    public interface ItemFragmentCallbacks {
        void onItemClicked(Item clickedItem);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ItemFragmentCallbacks) context;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
            Crashlytics.logException(ex);
            throw new ClassCastException(context.toString()
                    + " must implement ItemFragmentCallbacks");
        }
    }
}
