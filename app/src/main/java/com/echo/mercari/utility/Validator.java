package com.echo.mercari.utility;

import android.app.Activity;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.echo.mercari.R;

import java.util.regex.Pattern;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class Validator {

    public static String regexBoxNumber = "^[0-9]$";

    public static String regexDate = "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])";
    // private static String reqexPhone = "(((009|\\+9)|(?=.*[1-9]))).{9,18}";
    public static String reqexPhone = "((?=.*[1-9])).{9,13}";

    public static String regexTextArea = "^(?!\\s*$).+";
    public static String regexTransferNumber = "(?=.*[0-9])";
    public static String regexName = "^(?!\\s*$).+";
    public static String regexGender = "(male|female|Male|Female)";
    public static String regexTime = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
    public static String regexPrice = "^\\d{0,8}(\\.\\d{1,4})?$";


    // private static String regexYouTubeUrl = "^(https?\\:\\/\\/)?((www\\|m\\).youtube\\.com|youtu\\.?be)\\/.+$";
    public static String regexYouTubeUrl = "(?:https?:\\/\\/)?(?:(www|m)\\.)?youtu\\.?be(?:\\.com)?\\/?.*(?:watch|embed)?(?:.*v=|v\\/|\\/)([\\w\\-_]+)\\&?";
    ///private static String regexPrice = "^[1-9]\\d{0,7}(?:\\.\\d{1,4})?$";
    //private static String reqexPhone = "^(?:0|\\(?\\+966\\)?\\s?|00966\\s?)[1-79](?:[\\.\\-\\s]?\\d\\d){4}$";
//    public static String regexPassword = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{6,}";

    public static String regexPassword = "[0-9a-zA-Z~`!@#\\\\$%\\\\^&\\\\*\\\\(\\\\)\\\\-_\\\\+=\\\\{\\\\}\\\\[\\\\]\\\\|\\\\;:\\\"<>,./\\\\0-9]{6,}";
    AwesomeValidation mAwesomeValidation;
    Activity mContext;

    public Validator(Activity context, ValidationStyle validationStyle) {
        mAwesomeValidation = new AwesomeValidation(validationStyle);
        mContext = context;
    }

    public Validator(Activity context) {
        mAwesomeValidation = new AwesomeValidation(BASIC);
        mContext = context;
    }


    public void validateUserName(int id) {
        mAwesomeValidation.addValidation(mContext, id, regexName, R.string.err_name);
    }

    public void validateEditText(EditText editText, String pattert, int str) {
        mAwesomeValidation.addValidation(editText, Pattern.compile(pattert), mContext.getResources().getString(str));
    }

    public void validateEditText(EditText editText, Pattern pattert, int str) {
        mAwesomeValidation.addValidation(editText, pattert, mContext.getResources().getString(str));
    }

    public void validateYouTubeUrl(int id) {
        mAwesomeValidation.addValidation(mContext, id, regexYouTubeUrl, R.string.err_url);
    }

    public void validateEmail(int id) {
        mAwesomeValidation.addValidation(mContext, id, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);
    }

    public void validatePassword(int id) {
        mAwesomeValidation.addValidation(mContext, id, regexPassword, R.string.err_password);
    }

    public void validateConfirmPassword(int idPassword, int idConfirm) {
        mAwesomeValidation.addValidation(mContext, idConfirm, idPassword, R.string.err_password_confirmation);

    }

    public void validatePhone(int id) {
        mAwesomeValidation.addValidation(mContext, id, reqexPhone, R.string.err_phone);

    }


    public void validateDate(int id) {
        mAwesomeValidation.addValidation(mContext, id, regexDate, R.string.err_date);

    }


    public void validateTextArea(int id) {
        mAwesomeValidation.addValidation(mContext, id, regexTextArea, R.string.err_empty);

    }


    public boolean isValid() {
        return mAwesomeValidation.validate();
    }


}
