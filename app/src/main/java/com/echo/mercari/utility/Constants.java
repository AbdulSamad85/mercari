package com.echo.mercari.utility;

import android.os.Environment;

/**
 * Created by christian on 04/10/2016.
 */
public class Constants {

    public static String PREFS_NAME = "app_prefs";
    public static final String PROFILE_IMAGE_CAPTURE_PATH = Environment.getExternalStorageDirectory() + "/Mercari/picture";

}
