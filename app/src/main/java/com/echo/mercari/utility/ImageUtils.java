package com.echo.mercari.utility;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.echo.mercari.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by abdulsamad on 24/01/2017.
 */

public class ImageUtils {

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Context inContext, Uri uri) {
        Cursor cursor = null;
        try {
            cursor = inContext.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        } finally {
            cursor.close();
        }
    }

    private static boolean bitmapSmallerThanPreview(Bitmap bitmap, int previewWidth, int previewHeight) {
        return bitmap.getWidth() < previewWidth || bitmap.getHeight() < previewHeight;
    }

    private static boolean bitmapBiggerThanPreview(Bitmap bitmap, int previewWidth, int previewHeight) {
        return bitmap.getWidth() > previewWidth && bitmap.getHeight() > previewHeight;
    }

    public static Bitmap scaleBitmapToPreviewAspectRatio(Bitmap bitmap, int previewWidth, int previewHeight) {

        float ratioWidth = (float) bitmap.getWidth() / previewWidth;
        float ratioHeight = (float) bitmap.getHeight() / previewHeight;

        if (bitmapSmallerThanPreview(bitmap, previewWidth, previewHeight) ||
                bitmapBiggerThanPreview(bitmap, previewWidth, previewHeight)) {
            Log.d("Bitmap", "Bitmap scaling is required");

            int scaledWidth;
            int scaledHeight;
            if (ratioWidth < ratioHeight) {
                scaledWidth = previewWidth;
                scaledHeight = (int) (bitmap.getHeight() / ratioWidth);
            } else {
                scaledWidth = (int) (bitmap.getWidth() / ratioHeight);
                scaledHeight = previewHeight;
            }
            Bitmap scaledUpBitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, true);
            bitmap.recycle();
            bitmap = scaledUpBitmap;
        }

        if (Float.compare(ratioWidth, ratioHeight) == 0) {
            Log.d("Bitmap", "Aspect ration is the same between camera and preview. No need to crop");
            return bitmap;
        }

        ratioWidth = (float) bitmap.getWidth() / previewWidth;
        ratioHeight = (float) bitmap.getHeight() / previewHeight;

        int width;
        int x;
        int height;
        int y;
        if (ratioHeight < ratioWidth) {
            width = (int) ratioHeight * previewWidth;
            x = (bitmap.getWidth() - width) / 2;
            height = bitmap.getHeight();
            y = 0;
        } else {
            width = bitmap.getWidth();
            x = 0;
            height = (int) ratioWidth * previewHeight;
            y = (bitmap.getHeight() - height) / 2;
        }

        Log.d("Bitmap", "Cropping picture bitmap. x=" + x + ",y=" + y + ",width=" + width + ",height=" + height);
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
        bitmap.recycle();
        return croppedBitmap;
    }

    public static File createFile(Bitmap scaledBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File dir = new File(Constants.PROFILE_IMAGE_CAPTURE_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File destination = new File(dir,
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            Log.d("CreateFile", "Failure");
        }
        return destination;

    }

    public static boolean deleteFile(File file) {
        if (file != null && file.exists() && file.isFile()) {
            return file.delete();
        }
        return false;
    }

    public static Bitmap cropToSquareBitmap(Bitmap bitmap) {
        int x = 0;
        int y = 0;
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (bitmap.getHeight() > bitmap.getWidth()) {
            y = (bitmap.getHeight() - bitmap.getWidth()) / 2;
            height = bitmap.getWidth();
        } else {
            x = (bitmap.getWidth() - bitmap.getHeight()) / 2;
            width = bitmap.getHeight();
        }

        Log.d("Bitmap", "Crop bitmap. x=" + x + ",y=" + y + ",width=" + width + ",height=" + height);
        return Bitmap.createBitmap(bitmap, 20, 20, width - 30, height - 30);
    }

    public static Bitmap rotateBitmap(String selectedPath, Bitmap bitmap) {
        Bitmap imageBitmap = bitmap;

        try {

            ExifInterface exif = new ExifInterface(selectedPath);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            int deg = rotationInDegrees;
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
                imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);
                return imageBitmap;

            } else {
                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    public static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static Bitmap convert(String base64Str) throws IllegalArgumentException {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",") + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }


    public static String convert(File file){
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        return convert(bm);
    }
    public static String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static void loadImage(Context context, ImageView imageView, String uri, int width, int height) {
        loadImage(context, imageView, uri, width, height, false);
    }

    public static void loadImage(Context context, ImageView imageView, String url, int width, int height, boolean isCenterCrop) {
        if (url != null && url.length() > 0) {
            if (url.contains("graph.facebook.com")) {
                url = url.replaceAll("http", "https");
            }

            if(!url.contains("https") && url.contains("http")){
                url = url.replaceAll("http","https");
            }


            int widthDp = (int) pxFromDp(context, width);
            int heightDp = (int) pxFromDp(context, height);
            if (isCenterCrop) {
                Picasso.with(context).load(url).error(R.drawable.placeholder).centerCrop().resize(widthDp, heightDp).into(imageView);
            } else {
                Picasso.with(context).load(url).error(R.drawable.placeholder).resize(widthDp, heightDp).into(imageView);
            }
        }
    }

    public static void loadImage(Context context, ImageView imageView, String url) {
        if (url != null && url.length() > 0) {
            if (url.contains("graph.facebook.com")) {
                url = url.replaceAll("http", "https");
            }


            if(!url.contains("https") && url.contains("http")){
                url = url.replaceAll("http","https");
            }

            Picasso.with(context).load(url).error(R.drawable.placeholder).into(imageView);
        }
    }

    public static void loadImage(Context context, ImageView imageView, File uri, int width, int height) {
        int widthDp = (int) pxFromDp(context, width);
        int heightDp = (int) pxFromDp(context, height);

        Picasso.with(context).load(uri).error(R.drawable.placeholder).resize(widthDp, heightDp).into(imageView);
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static void loadResizedImage(Context context, ImageView imageView, String url, int width, int height) {
        if (!TextUtils.isEmpty(url)) {

            if (url.contains("graph.facebook.com")) {
                url = url.replaceAll("http", "https");
            }
            int widthDp = (int) pxFromDp(context, width);
            int heightDp = (int) pxFromDp(context, height);

            if(!url.contains("https") && url.contains("http")){
                url = url.replaceAll("http","https");
            }

            Picasso.with(context).load(url).error(R.drawable.placeholder).resize(widthDp, heightDp).into(imageView);
        }
    }

    public static void loadResizedImage(Context context, ImageView imageView, File imageFile, int width, int height) {
        Picasso.with(context).load(imageFile).error(R.drawable.placeholder).resize(width, height).into(imageView);
    }

    public static void loadCircularImage(Context context, ImageView imageView, String url, int width, int height) {

        if(!url.contains("https") && url.contains("http")){
            url = url.replaceAll("http","https");
        }
        int widthDp = (int) pxFromDp(context, width);
        int heightDp = (int) pxFromDp(context, height);
        if (url.contains("graph.facebook.com")) {
            url = url.replaceAll("http", "https");
        }

        Picasso.with(context)
                .load(url)
                .resize(widthDp, heightDp)
                .centerCrop()
                .transform(new CircleTransform())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(imageView);
    }

//    public static Bitmap fastblurNew(Context context, Bitmap sentBitmap) {
//        try {
//            final RenderScript rs = RenderScript.create(context);
//            final Allocation input = Allocation.createFromBitmap(rs, sentBitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
//            final Allocation output = Allocation.createTyped(rs, input.getType());
//            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//            script.setRadius(3.f /* e.g. 3.f */);
//            script.setInput(input);
//            script.forEach(output);
//            output.copyTo(sentBitmap);
//            return sentBitmap;
//        } catch (RuntimeException ex) {
//            // Picasso throws exception with the Blur transformation for some devices, avoiding the crash
//            // handled exception
//            return sentBitmap;
//        }
//    }

    /**
     * Stack Blur v1.0 from
     * http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
     * Java Author: Mario Klingemann <mario at quasimondo.com>
     * http://incubator.quasimondo.com
     * <p>
     * created Feburary 29, 2004
     * Android port : Yahel Bouaziz <yahel at kayenko.com>
     * http://www.kayenko.com
     * ported april 5th, 2012
     * <p>
     * This is a compromise between Gaussian Blur and Box blur
     * It creates much better looking blurs than Box Blur, but is
     * 7x faster than my Gaussian Blur implementation.
     * <p>
     * I called it Stack Blur because this describes best how this
     * filter works internally: it creates a kind of moving stack
     * of colors whilst scanning through the image. Thereby it
     * just has to add one new block of color to the right side
     * of the stack and remove the leftmost color. The remaining
     * colors on the topmost layer of the stack are either added on
     * or reduced by one, depending on if they are on the right or
     * on the left side of the stack.
     * <p>
     * If you are using this algorithm in your code please add
     * the following line:
     * Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>
     */

//    public static Bitmap fastblur(Bitmap sentBitmap) {
//
//        float scale = 0.8f;
//        int radius = 7;
//        int width = Math.round(sentBitmap.getWidth() * scale);
//        int height = Math.round(sentBitmap.getHeight() * scale);
//        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);
//
//        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
//
//        if (radius < 1) {
//            return (null);
//        }
//
//        int w = bitmap.getWidth();
//        int h = bitmap.getHeight();
//
//        int[] pix = new int[w * h];
//        bitmap.getPixels(pix, 0, w, 0, 0, w, h);
//
//        int wm = w - 1;
//        int hm = h - 1;
//        int wh = w * h;
//        int div = radius + radius + 1;
//
//        int r[] = new int[wh];
//        int g[] = new int[wh];
//        int b[] = new int[wh];
//        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
//        int vmin[] = new int[Math.max(w, h)];
//
//        int divsum = (div + 1) >> 1;
//        divsum *= divsum;
//        int dv[] = new int[256 * divsum];
//        for (i = 0; i < 256 * divsum; i++) {
//            dv[i] = (i / divsum);
//        }
//
//        yw = yi = 0;
//
//        int[][] stack = new int[div][3];
//        int stackpointer;
//        int stackstart;
//        int[] sir;
//        int rbs;
//        int r1 = radius + 1;
//        int routsum, goutsum, boutsum;
//        int rinsum, ginsum, binsum;
//
//        for (y = 0; y < h; y++) {
//            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
//            for (i = -radius; i <= radius; i++) {
//                p = pix[yi + Math.min(wm, Math.max(i, 0))];
//                sir = stack[i + radius];
//                sir[0] = (p & 0xff0000) >> 16;
//                sir[1] = (p & 0x00ff00) >> 8;
//                sir[2] = (p & 0x0000ff);
//                rbs = r1 - Math.abs(i);
//                rsum += sir[0] * rbs;
//                gsum += sir[1] * rbs;
//                bsum += sir[2] * rbs;
//                if (i > 0) {
//                    rinsum += sir[0];
//                    ginsum += sir[1];
//                    binsum += sir[2];
//                } else {
//                    routsum += sir[0];
//                    goutsum += sir[1];
//                    boutsum += sir[2];
//                }
//            }
//            stackpointer = radius;
//
//            for (x = 0; x < w; x++) {
//
//                r[yi] = dv[rsum];
//                g[yi] = dv[gsum];
//                b[yi] = dv[bsum];
//
//                rsum -= routsum;
//                gsum -= goutsum;
//                bsum -= boutsum;
//
//                stackstart = stackpointer - radius + div;
//                sir = stack[stackstart % div];
//
//                routsum -= sir[0];
//                goutsum -= sir[1];
//                boutsum -= sir[2];
//
//                if (y == 0) {
//                    vmin[x] = Math.min(x + radius + 1, wm);
//                }
//                p = pix[yw + vmin[x]];
//
//                sir[0] = (p & 0xff0000) >> 16;
//                sir[1] = (p & 0x00ff00) >> 8;
//                sir[2] = (p & 0x0000ff);
//
//                rinsum += sir[0];
//                ginsum += sir[1];
//                binsum += sir[2];
//
//                rsum += rinsum;
//                gsum += ginsum;
//                bsum += binsum;
//
//                stackpointer = (stackpointer + 1) % div;
//                sir = stack[(stackpointer) % div];
//
//                routsum += sir[0];
//                goutsum += sir[1];
//                boutsum += sir[2];
//
//                rinsum -= sir[0];
//                ginsum -= sir[1];
//                binsum -= sir[2];
//
//                yi++;
//            }
//            yw += w;
//        }
//        for (x = 0; x < w; x++) {
//            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
//            yp = -radius * w;
//            for (i = -radius; i <= radius; i++) {
//                yi = Math.max(0, yp) + x;
//
//                sir = stack[i + radius];
//
//                sir[0] = r[yi];
//                sir[1] = g[yi];
//                sir[2] = b[yi];
//
//                rbs = r1 - Math.abs(i);
//
//                rsum += r[yi] * rbs;
//                gsum += g[yi] * rbs;
//                bsum += b[yi] * rbs;
//
//                if (i > 0) {
//                    rinsum += sir[0];
//                    ginsum += sir[1];
//                    binsum += sir[2];
//                } else {
//                    routsum += sir[0];
//                    goutsum += sir[1];
//                    boutsum += sir[2];
//                }
//
//                if (i < hm) {
//                    yp += w;
//                }
//            }
//            yi = x;
//            stackpointer = radius;
//            for (y = 0; y < h; y++) {
//                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
//                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];
//
//                rsum -= routsum;
//                gsum -= goutsum;
//                bsum -= boutsum;
//
//                stackstart = stackpointer - radius + div;
//                sir = stack[stackstart % div];
//
//                routsum -= sir[0];
//                goutsum -= sir[1];
//                boutsum -= sir[2];
//
//                if (x == 0) {
//                    vmin[y] = Math.min(y + r1, hm) * w;
//                }
//                p = x + vmin[y];
//
//                sir[0] = r[p];
//                sir[1] = g[p];
//                sir[2] = b[p];
//
//                rinsum += sir[0];
//                ginsum += sir[1];
//                binsum += sir[2];
//
//                rsum += rinsum;
//                gsum += ginsum;
//                bsum += binsum;
//
//                stackpointer = (stackpointer + 1) % div;
//                sir = stack[stackpointer];
//
//                routsum += sir[0];
//                goutsum += sir[1];
//                boutsum += sir[2];
//
//                rinsum -= sir[0];
//                ginsum -= sir[1];
//                binsum -= sir[2];
//
//                yi += w;
//            }
//        }
//
//        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
//
//        return (bitmap);
//    }

    public static Drawable getVectorDrawable(Context context, int resId) {
        VectorDrawableCompat drawable;
        try {
            drawable = VectorDrawableCompat.create(context.getResources(), resId, context.getTheme());
        } catch (Exception e) {
            e.printStackTrace();
            return new ColorDrawable(Color.TRANSPARENT);
        }

        if (drawable != null)
            return drawable.getCurrent();
        else
            return new ColorDrawable(Color.TRANSPARENT);
    }
}