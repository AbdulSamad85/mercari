package com.echo.mercari.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.echo.mercari.MyApplication;
import com.google.gson.Gson;

/**
 * Created by zaid on 4/30/2015.
 */
public class SharedPrefUtility {

    private SharedPreferences sharedPreferences;
    private static SharedPrefUtility utility;

    private SharedPrefUtility(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = (context).getSharedPreferences(
                    Constants.PREFS_NAME, Context.MODE_PRIVATE);
        }
    }

    public static synchronized SharedPrefUtility getInstance(Context c) {
        if (utility == null) {
            utility = new SharedPrefUtility(c);
        }
        return utility;
    }

    public SharedPreferences getPreferences() {
        return sharedPreferences;
    }

    public void savePrefrences(String key, String value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putString(key, value).commit();
        }
    }

    public void savePrefrences(String key, int value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putInt(key, value).commit();
        }
    }

    public void savePrefrences(String key, boolean value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putBoolean(key, value).commit();
        }
    }

    public void savePrefrences(String key, long value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putLong(key, value).commit();
        }
    }

    public void savePrefrences(String key, Object object) {
        if (sharedPreferences != null) {
            Gson gson = new Gson();
            String objectJson = gson.toJson(object);
            SharedPrefUtility.getInstance(MyApplication.getAppContext()).savePrefrences(key, objectJson);
        }
    }


//    public Object get(String key, Class<Form> objectClass) {
//            String json = SharedPrefUtility.getInstance(MyApplication.getAppContext()).getStringValue(key);
//            if (!TextUtils.isEmpty(json)) {
//                Gson gson = new Gson();
//                return gson.fromJson(json, objectClass);
//            } else {
//                return null;
//            }
//    }

    public String getStringValue(String key) {
        return sharedPreferences.getString(key, null);
    }

    public boolean getBooleanValue(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public int getIntValue(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public long getLongValue(String key) {
        return sharedPreferences.getLong(key, -1);
    }



}
