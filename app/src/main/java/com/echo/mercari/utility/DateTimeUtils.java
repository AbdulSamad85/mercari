package com.echo.mercari.utility;

//import com.crashlytics.android.Crashlytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by abdulsamad on 1/1/18.
 */

public class DateTimeUtils {

//    /**
//     * converts "31-12-2017 23:00:00" to ETC/UTC date object
//     *
//     * @param dateString
//     * @return
//     */
//    public static String convertToLocalDateString(String dateString) {
//        SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
//        inputDateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
//
//
//        SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//        outputDateFormat.setTimeZone(TimeZone.getDefault());
//        String convertedDateString = "";
//        try {
//            Date date = inputDateFormat.parse(dateString);
//            convertedDateString = outputDateFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            Crashlytics.logException(e);
//        }
//        return convertedDateString;
//    }


    /**
     * converts a UTC/ETC date string ("31-12-2017 23:00:00") to local Calendar object
     *
     * @param dateString
     * @return
     */
    public static Calendar convertToLocalCalendar(String dateString) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
        inputDateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));


        SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
        outputDateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calConvertedTime = Calendar.getInstance();
        calConvertedTime.clear();
        try {
            Date date = inputDateFormat.parse(dateString);
            String convertedDateString = outputDateFormat.format(date);
            calConvertedTime.setTime(outputDateFormat.parse(convertedDateString));
        } catch (ParseException e) {
            e.printStackTrace();
//            Crashlytics.logException(e);
        }
        return calConvertedTime;
    }


    /**
     * To be used for hashmap keys to store time slots under  particular date object
     *
     * @param gmtDateString
     * @return
     */
    public static String convertToLocalDateString(String gmtDateString) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "dd-MM-yyyy HH:mm:ss", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String outputText = null + "";
        Calendar calendar = Calendar.getInstance();

        try {
            Date date = inputFormat.parse(gmtDateString);
            outputText = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
//            Crashlytics.logException(e);
        }
        return outputText;
    }

    /**
     * converts a UTC date object to local date String (wiht device's default local)
     *
     * @param date
     * @return
     */
    public static String convertToString(Date date) {
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
// Adjust locale and zone appropriately
        return outputFormat.format(date);
    }

}
