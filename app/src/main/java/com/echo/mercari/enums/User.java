package com.echo.mercari.enums;

/**
 * Created by abdulsamad on 11/03/2018.
 */

public enum User {
    MEN,
    WOMEN,
    ALL
}
