package com.echo.mercari.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.echo.mercari.R;
import com.echo.mercari.models.Item;
import com.echo.mercari.utility.ImageUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by abdulsamad on 12/01/2017.
 */
public class ItemGridAdapter extends BaseAdapter {

    private List<Item> dataList;
    private LayoutInflater inflater;
    private Context context;
    private NumberFormat format;


    // Constructor
    public ItemGridAdapter(Context context, List<Item> dataList) {
        this.context = context;
        this.dataList = new ArrayList<>();
        this.dataList.addAll(dataList);
        format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setMaximumFractionDigits(0);
        this.inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return dataList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView image;
        ImageView soldOutOverlay;
        TextView likeCount, commentCount, price, title;

        Item item = dataList.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_item_layout, null);

            viewHolder.image = convertView.findViewById(R.id.image);
            viewHolder.likeCount = convertView.findViewById(R.id.likeCount);
            viewHolder.commentCount = convertView.findViewById(R.id.commentCount);
            viewHolder.price = convertView.findViewById(R.id.price);
            viewHolder.title = convertView.findViewById(R.id.title);
            viewHolder.soldOutOverlay = convertView.findViewById(R.id.soldPutOverlay);

            convertView.setTag(viewHolder);
        } else {
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.likeCount.setText(item.getNumLikes()+"");
        viewHolder.commentCount.setText(item.getNumComments()+"");
        viewHolder.title.setText(item.getName());
        viewHolder.price.setText(format.format(item.getPrice()));

        if (item.getStatus() != null && item.getStatus().equalsIgnoreCase(context.getString(R.string.sold_out))) {
            viewHolder.soldOutOverlay.setVisibility(View.VISIBLE);
        } else {
            viewHolder.soldOutOverlay.setVisibility(View.GONE);
        }

        ImageUtils.loadResizedImage(context, viewHolder.image, item.getPhoto(), 0, 170);
        convertView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.item_animation_from_bottom));
        return convertView;
    }

    private static class ViewHolder{
        public ImageView image, soldOutOverlay;
        public TextView likeCount, commentCount, price, title;
    }
}

