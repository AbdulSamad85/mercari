package com.echo.mercari.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.echo.mercari.fragment.ItemGridFragment.ItemGridFragment;
import com.echo.mercari.models.Item;
import java.util.List;

/**
 * Created by abdulsamad on 15/03/2018.
 */

public class SimpleFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> titlesList;
    private List<List<Item>> dataSource;


    public SimpleFragmentPagerAdapter(FragmentManager fm, List<String> titlesList, List<List<Item>> dataSource) {
        super(fm);
        this.titlesList = titlesList;
        this.dataSource = dataSource;
    }

    @Override
    public int getCount() {
        return titlesList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return ItemGridFragment.newInstance(dataSource.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titlesList.get(position);
    }
}
