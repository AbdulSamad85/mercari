package com.echo.mercari.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.echo.mercari.R;


/**
 * Created by Abdul Samad on 30/12/2015.
 */
public class CustomFontHelper {

    /**
     * sets custom font when. this will show custom font on device but not in IDE layout editor
     * Font this would try to get font from the attributes if not found this would set custom appFont
     * @param context
     * @param attrs can be null
     */
    public static void setCustomFont (TextView view, Context context, @Nullable AttributeSet attrs) {
        if (view.isInEditMode()) return; // do not show custom font in edit mode (in IDE-layout editor)

        String fontAsset = context.getString(R.string.appFont);
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FontText);
            if (ta != null) {
                String fontAssetFromAttrs = ta.getString(R.styleable.FontText_typefaceAsset);
                if (fontAssetFromAttrs != null && !fontAssetFromAttrs.isEmpty()) {
                    fontAsset = fontAssetFromAttrs;
                }
                ta.recycle();
            }
        }
        Typeface font = FontCache.get(context, fontAsset);
        if (font != null) view.setTypeface(font);
    }
}
