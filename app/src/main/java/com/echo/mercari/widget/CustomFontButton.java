package com.echo.mercari.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;


/**
 * Creates Button with custom font.
 * Created by Abdul Samad on 30/12/2015.
 */
public class CustomFontButton extends Button {

    public CustomFontButton(Context context) {
        super(context);
        setCustomFont(context, null);
    }
    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }
    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    /**
     * sets custom font when. this will show custom font on device but not in IDE layout editor
     * Font this would try to get font from the attributes if not found this would set custom appFont
     * @param context
     * @param attrs can be null
     */
    private void setCustomFont (Context context, AttributeSet attrs) {
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}
