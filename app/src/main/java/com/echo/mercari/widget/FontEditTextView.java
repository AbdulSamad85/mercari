package com.echo.mercari.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;


/**
 * Created by Abdul Samad on 30/12/2015.
 */
public class FontEditTextView extends EditText {

    public FontEditTextView(Context context) {
        super(context);
        setCustomFont(context, null);
    }
    public FontEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }
    public FontEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FontEditTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setCustomFont(context, attrs);
    }

    /**
     * sets custom font when. this will show custom font on device but not in IDE layout editor
     * Font this would try to get font from the attributes if not found this would set custom appFont
     * @param context
     * @param attrs can be null
     */
    private void setCustomFont (Context context, AttributeSet attrs) {
        CustomFontHelper.setCustomFont(this, context, attrs);
    }


}
