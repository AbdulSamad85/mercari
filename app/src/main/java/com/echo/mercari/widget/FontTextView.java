package com.echo.mercari.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;


public class FontTextView extends TextView {

    public FontTextView(Context context) {
        super(context);
        setCustomFont(context, null);
    }
    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }
    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    /**
     * sets custom font when. this will show custom font on device but not in IDE layout editor
     * Font this would try to get font from the attributes if not found this would set custom appFont
     * @param context
     * @param attrs can be null
     */
    private void setCustomFont (Context context, AttributeSet attrs) {
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}