package com.echo.mercari.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.echo.mercari.R;


public class FontTextViewExtraBold extends TextView {

    public FontTextViewExtraBold(Context context) {
        super(context);
        setCustomFont(context);
    }

    public FontTextViewExtraBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }
    public FontTextViewExtraBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context);
    }

    /**
     * sets custom font when. this will show custom font on device but not in IDE layout editor
     */
    private void setCustomFont (Context context) {
        if (isInEditMode()) return; // do not show custom font in edit mode (in IDE-layout editor)
        Typeface font = FontCache.get(context, context.getString(R.string.appFontExtraBold));
        if (font != null) this.setTypeface(font);
    }
}