package com.echo.mercari;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        initFabric();
        context = this;
    }

    private void initFabric(){
        Fabric.with(this, new Crashlytics());
    }

    public static Context getAppContext() {
        return context;
    }
}
